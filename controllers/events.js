import "regenerator-runtime/runtime.js";
import Event from "../models/Event.js";
import mongoose from "mongoose";


export const getEvents = async (req, res) => {
    try {
        const events = await Event.find();
        res.status(200).json(events);
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

export const createEvent = async (req, res) => {

    const event = req.body;
    const newEvent = new Event(event);

    try {
        await newEvent.save();
        res.status(201).json(newEvent)
    } catch (error) {
        res.status(401).json({message: error.message})
    }
}

export const updateEvent = async (req, res) => {
    const {id} = req.params;
    const {title, message, link, selectedFile} = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

    const updatedPost = {
        title,
        message,
        link,
        selectedFile,
        _id: id
    };

    await Event.findByIdAndUpdate(id, updatedPost, {new: true});

    res.json(updatedPost);
}

export const deleteEvent = async (req, res) => {
    const {id} = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

    await Event.findByIdAndRemove(id);

    res.json({message: 'Post deleted successfully'})
}