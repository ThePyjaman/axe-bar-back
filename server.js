import express from "express"
import bodyParser from "body-parser"
import mongoose from "mongoose"
import cors from "cors"
import eventRoutes from './routes/events.js'
import path from 'path'

const corsOptions = {
    origin: 'www.thepyjaman.com',
    optionsSuccessStatus: 200
}

const __dirname = path.resolve(path.dirname(''));
const app = express();


app.use(bodyParser.json({limit: "30mb", extended: true}));
app.use(bodyParser.urlencoded({limit: "30mb", extended: true}));
app.use(cors());
app.use(express.static(__dirname + '/public'))

app.use('/events', eventRoutes, cors(corsOptions));

app.use('/events', eventRoutes);
const CONNECTION_URL = 'mongodb+srv://thepyjaman:7eSzbqZWSmTYPqFR@cluster0.o7fbv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
const PORT = process.env.PORT || 5000;

mongoose.connect(CONNECTION_URL, {useNewUrlParser: true, useUnifiedTopology: true}).then(() => {
    app.listen(PORT, () => console.log(`Server running on ${PORT}`))
})




mongoose.set('useFindAndModify', false);

